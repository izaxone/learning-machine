# Learning Machine

"Without Warren Buffet being a continuous leanring machine, the record would have been absolutely impossible." -Charlie Munger

The learning machine is powered by three amazing pieces of technology: [Markdown](https://daringfireball.net/projects/markdown/), [Anki](https://apps.ankiweb.net/), and the Markdown to Anki converter: [mdanki](https://github.com/ashlinchak/mdanki). 

## Setup
1. Install required software: [Node.js](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm),[Anki](https://apps.ankiweb.net/), and [mdanki](https://github.com/ashlinchak/mdanki#install)
2. Create a new Markdown file called `questions.md`, and place the file `create-apkg.sh` in the same directory
3. Add your questions in `questions.md` according to the `mdanki` formatting (see the Cards headline below)
4. Run `create-apkg.sh`, then [import the created .apkg file to Anki](https://docs.ankiweb.net/exporting.html#packaged-decks)
5. Use the [Remove Duplicate Cards](https://ankiweb.net/shared/info/95590040) Anki Add-on to remove duplicate cards. 
## Cards

By default, MDAnki splits cards by `## ` headline. For example, below markdown will generate 2 cards where headlines will be on the front side and its description - on the back.

```
## Question 1

Answer 1

Question 2

Answer 2

```

If you want to have multiple lines on the card's front side - use `%` symbol for splitting front and back sides:

```
## Question 1

Answer 1

%

Another line for answer 1

```

When parsing only one markdown file, the title of the deck could be generated based on the top-level headline (`# `).

*For more information, please see [README.md on the mdanki repository](https://github.com/ashlinchak/mdanki/blob/master/README.md)*